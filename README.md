# Game Tournament Maker

An automatic game tournament maker, with a voice announcer!

## Features

Generates a **round-based** gaming tournament:

- Each round, a team matchup and a game will be randomly picked from a pre-configured pool.
- The chosen teams and game are then removed from the pool, to prevent them from repeating.
- Whenever the team pool or game pool are empty, they automatically replenish.
- The tournament will generate new rounds infinitely, until players decide to stop.

**Other cool features:**

- Displays the tournament progress in fancy ASCII art.
- Announces chosen teams and games with text-to-speech.

## Limitations

- Does not support the bracket tournament style.
- Does not record round scores, you will need to handle that separately.
- Only Windows 10/11 are supported.

## Before You Start

1. Edit [game_pool.conf](config/game_pool.conf) and put in the games of your tournament.
1. Edit [team_pool.conf](config/team_pool.conf) and put in the teams of your tournament.

## How to Start

Double-click on `play.bat`

## License

Please see the [LICENSE](LICENSE) file.

## Attribution

### Tool/Library Attribution

- ASCII art generation is made possible by the [FIGlet](http://www.figlet.org/) program and this online [FIGlet API](https://helloacm.com/figlet/).

### Logo Attribution

<div>Icons made by <a href="https://www.flaticon.com/authors/smashicons" title="Smashicons">Smashicons</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
