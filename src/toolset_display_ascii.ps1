function Show-AsciiArt(
    [string] $Text,
    [string] $Color)
{
    $uri = "https://uploadbeta.com/api/figlet/?cached&msg=$Text"
    $response = Invoke-WebRequest -Uri $uri -UseBasicParsing
    $response = $response | ConvertFrom-Json
    Write-Host $response -ForegroundColor $Color

    if($global:SpeechSynthesizer)
    {
        $global:SpeechSynthesizer.Speak($Text)
    }
}
